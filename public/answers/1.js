function Task()  {
	var self = this;
	var storage = {};

	this.input = document.getElementById('input');
	this.output = document.getElementById('output');

	// Проверка формата и парсинг
	function contentParse(type,content) {
		var checkFormat = type.match(/\.\w+$/);
		var format = checkFormat[0].substr(1);
		var parser = {
			json : function(data) {
				var result = !data ? 'error json' : JSON.parse(data);
				return result.text;
			},
			js : function(data) {

				window.cb = function(text){
	                return text;
	            };
	            var result = eval(data);
        		delete window.cb;
		        return result.text;
			},
			txt : function(data) {
				return data;
			}
		}
		try {
			return parser[format](content);
		} catch(e) {
			return e;
		};
	}

	// Получение файла
	function getFile(url) {
		
		// не только дройды. Джедаи тоже их используют
		return new Promise(function(resolve, reject) {

			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true);

			xhr.onload = function() {
				if (this.readyState != 4) return;	

				if (this.status == 200 && resolve) {
					resolve(this.responseText);
				} else {
                   reject();
                }

			};
			xhr.send();
		});
	}

	// Обновление данных в textarea
	function setOutput(result) {
		self.output.value = result;
	}

	// публичный метод.
	this.update = function(url) {
		if (storage[url]){
            return setOutput(storage[url]);
        }

		setOutput('Загрузка...');

		var promise = getFile(url);

		promise.then(function(response) {
			var result = contentParse(url,response); // проверяем контент
			storage[url] = result; // кэшируем
			setOutput(result); // показываем результат
		});

		promise.catch(function(){
	        setOutput('Ошибка...');
	    });
		
	}
};

var task = new Task();

task.input.onchange = function (event) {
	task.update(task.input.value);
};
window.onload = function() {
    task.update(task.input.value);
};